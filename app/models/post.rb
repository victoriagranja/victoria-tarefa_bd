class Post < ApplicationRecord
    validates:name, presence:true
    validates:message, presence:true
    has_many :likes
    has_many :comments
end
